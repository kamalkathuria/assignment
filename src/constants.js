export default {
  PACKAGE_THRESHOLD: 250,
  FETCH_PRODUCTS_API: "http://localhost:3000/api/products",
  FETCH_COURIER_CHARGES_API: "http://localhost:3000/api/courier-charges"
}