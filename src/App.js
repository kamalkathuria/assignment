import React, { Component } from 'react';
import './App.css';
import Constants from './constants';
import Header from './components/Header/Header';
import ProductList from './components/ProductList/ProductList';
import Cart from './components/Cart/Cart';
import OrderPackage from './components/OrderPackage/OrderPackage';

class App extends Component {

  state = {
    products: [],
    courierCharges: {},
    packages: []
  }

  componentDidMount() {
    this.fetchProducts();
    this.fetchCourierCharges();
  }

  createPackages = () => {
    const selectedProducts = this.state.products.filter(product => product.selected);
    const sortedProducts = selectedProducts.sort((a, b) => a.price > b.price ? 1 : -1);
    const packages = [];
    let pack = [];
    let sum = 0;
    for (let i = 0; i < sortedProducts.length; i++) {
      const product = sortedProducts[i];
      sum += product.price;
      if (sum <= Constants.PACKAGE_THRESHOLD) {
        pack.push(product);
      } if (sum > Constants.PACKAGE_THRESHOLD || i === sortedProducts.length - 1) {
        sum = 0;
        packages.push(pack);
        pack = [];
        if (i < sortedProducts.length - 1) {
          i--;
        } else {
          pack.push(product);
          packages.push(pack);
        }
      }
    }
    this.setState({
      packages
    });
  }

  fetchProducts() {
    fetch(Constants.FETCH_PRODUCTS_API).then(res => res.json()).then(products => {
      this.setState({
        products
      });
    });
  }

  fetchCourierCharges() {
    fetch(Constants.FETCH_COURIER_CHARGES_API).then(res => res.json()).then(courierCharges => {
      this.setState({
        courierCharges
      });
    });
  }

  handleProductSelection = (e) => {
    const id = Number(e.target.id);
    const products = this.state.products.map(product => {
      return product.id === id ? Object.assign({}, product, { selected: !product.selected }) : product;
    })
    this.setState({
      products
    });
  }

  render() {
    return (
      <div className="container">
        <Header />
        <Cart
          products={this.state.products}
          createPackages={this.createPackages}
        />
        <OrderPackage
          packages={this.state.packages}
        />
        <ProductList
          handleProductSelection={this.handleProductSelection}
          products={this.state.products}
        />
      </div>
    );
  }
}

export default App;
