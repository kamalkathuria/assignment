import React, { Component } from 'react'
import './Cart.css';

export default class Cart extends Component {

  getSelectedProducts = () => {
    return this.props.products.map(product => {
      if (product.selected) {
        return (
          <div key={product.id} className="cart-item d-inline-block">
            <button className="btn btn-default">{product.name} - ${product.price}</button>
          </div>
        );
      }
      return null;
    })
  }

  isCartEmpty() {
    return this.props.products.filter(product => product.selected).length === 0;
  }

  render() {
    if (!this.isCartEmpty()) {
      return (
        <div className="row cart-container">
          <div className="col-12">
            {this.getSelectedProducts()}
            <button onClick={this.props.createPackages} className="btn btn-warning float-right package-btn">
              Package Order
            </button>
          </div>
        </div >
      );
    }
    return null;
  }
}
