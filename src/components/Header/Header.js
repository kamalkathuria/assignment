import React, { Component } from 'react'
import './Header.css';

export default class Header extends Component {
  render() {
    return (
      <div className="row header-container">
        <div className="col-12">
          <h1 className="d-inline-block">My Kart</h1>
        </div>
      </div>
    )
  }
}
