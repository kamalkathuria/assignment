import React, { Component } from 'react'
import './ProductList.css';
import Product from './Product/Product';

export default class ProductList extends Component {

  getProductList() {
    return this.props.products.map(product => {
      return <Product
        key={product.id}
        product={product}
        handleProductSelection={this.props.handleProductSelection}
      />;
    });
  }

  render() {
    return (
      <div className="row">
        {this.getProductList()}
      </div>
    );
  }
}
