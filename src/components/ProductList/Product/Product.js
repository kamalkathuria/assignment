import React, { Component } from 'react'
import './Product.css';

export default class Product extends Component {
  render() {
    const product = this.props.product;
    return (
      <div className="card col-md-4 col-xl-3">
        <div className="product-container">
          <img className="card-img-bottom img-fluid" src={product.imageURL} alt={product.name} />
          <div className="card-body">
            <h4 className="card-title">{product.name}</h4>
            <p className="card-text">${product.price}</p>
            <button onClick={this.props.handleProductSelection} id={product.id} className={product.selected ? "btn btn-secondary" : "btn btn-primary"}>{product.selected ? "Added to Cart" : "Add to Cart"}</button>
          </div>
        </div>
      </div>
    )
  }
}
