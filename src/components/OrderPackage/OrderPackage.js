import React, { Component } from 'react'
import './OrderPackage.css';

export default class OrderPackage extends Component {

  getPackages = () => {
    return this.props.packages.map(pack => {
      return (
        <div key={pack[0].id} className="col-12 package">
          {this.getProducts(pack)}
        </div>
      );
    });
  }

  getProducts = (pack) => {
    return pack.map(product => {
      return (
        <div key={product.id} className="package-item d-inline-block">
          <button className="btn btn-default">{product.name} - ${product.price}</button>
        </div>
      );
    });
  }

  render() {
    if (this.props.packages) {
      return (
        <div className="row packages-container">
          {this.getPackages()}
        </div>
      );
    }
    return null;
  }
}
